require("./config/config")
const mongoose = require('mongoose');
const express = require('express')
const body = require('body-parser')

const app = express();

// parse application/x-www-form-urlencoded
app.use(body.urlencoded({
  extended: false
}))
// parse application/json
app.use(body.json());
//Habilitar el publi
app.use(express.static(`${__dirname}/../public`));
//configuracion global de rutas

app.use(require('./routes/index.js'));



mongoose.connect(process.env.DB, {
    useNewUrlParser: true,
    useFindAndModify: false,
    useCreateIndex: true
  },
  (err, res) => {
    if (err) throw err;
    console.log(`Base de datos online en ${process.env.DB}`);
  });

app.listen(process.env.PORT, () => console.log(`Escuchando en el puerto ${process.env.PORT}`));
