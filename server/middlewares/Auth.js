var jwt = require('jsonwebtoken');
const Usuario = require('../models/usuario.js')


//==================================
//verificar token
//=================================

let verificarToken = (req,res,next)=> {
    let token = req.get('token');
    //res.json({token});
    jwt.verify(token,process.env.SEED,(err,decoded)=>{
        if(err){
            return res.status(401).json({
                ok:false,
                err
            })
        }
        req.usuario = decoded.usuario;
        next();
    })

}
let verificarPermisoAdmin = (req,res,next)=>{
    let token = req.get('token');
    if(req.usuario.role !== 'ADMIN_ROLE'){
        return res.status(401).json({
            ok:false,
            err:{
                message:'Error de no tiene permisos para agregar un usuario'
            }
        })
    }else{
        next();
    }
}

module.exports = {
    verificarToken,
    verificarPermisoAdmin
}
