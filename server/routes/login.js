const express = require('express')
const Usuario = require('../models/usuario.js')
const bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');

const {
  OAuth2Client
} = require('google-auth-library');
const client = new OAuth2Client(process.env.CLIENT_ID);

const app = express()
app.post('/login', (req, res) => {
  let body = req.body;
  Usuario.findOne({
    email: body.email
  }, (err, userDB) => {
    if (err) {
      return res.status(400).json({
        ok: false,
        err
      })
    }
    if (!userDB) {
      return res.status(400).json({
        ok: false,
        err: {
          message: 'Usuario o Contraseña incorrectos'
        }
      })
    }
    if (!bcrypt.compareSync(body.pass, userDB.pass)) {
      return res.status(400).json({
        ok: false,
        err: {
          message: 'Usuario o Contraseña incorrectos'
        }
      })
    }
    let token = jwt.sign({
      usuario: userDB
    }, process.env.SEED, {
      expiresIn: process.env.VENCIMIENTO
    });
    res.status(200).json({
      ok: true,
      usuario: userDB,
      token
    })
  })

})
//Configuraciones de google

async function verify(token) {
  const ticket = await client.verifyIdToken({
    idToken: token,
    audience: process.env.CLIENT_ID
  });
  const payload = ticket.getPayload();

  return {
    nombre: payload.name,
    email: payload.email,
    img: payload.picture,
    google: true
  };
  // If request specified a G Suite domain:
  //const domain = payload['hd'];
}

app.post('/google', async (req, res) => {
  let token = req.body.idToken;
  let googleUser = await verify(token).catch(e => res.status(403).json({
    okey: false,
    err: e
  }))
  Usuario.findOne({
    email: googleUser.email
  }, (err, userDB) => {
    if (err) {
      return res.status(500).json({
        ok: false,
        err
      })
    }
    if (userDB) {
      if (userDB.google === false) {
        return res.status(400).json({
          ok: false,
          erro: {
            msg: 'Debe user su autenticacion normal'
          }
        })
      } else {
        let token = encrip(userDB);
        return res.json({
          okey: true,
          usuario: userDB,
          token
        })
      }
    } else {
      //Su primer sign
      let usuario = new Usuario();
      usuario.nombre = googleUser.nombre;
      usuario.email = googleUser.email;
      usuario.img = googleUser.img;
      usuario.google = googleUser.true;
      usuario.password = ":)";
      usuario.save((err, usuarioDB) => {
        if (err) {
          return res.status(500).json({
            ok: false,
            err
          })
        }
      })
      let token = encrip(usuarioDB);
      return res.json({
        okey: true,
        usuario: userDB,
        token
      })
    }

  });

  res.json({
    googleUser
  })
});

function encrip(usuario) {
  return jwt.sign({
    usuario
  }, process.env.SEED, {
    expiresIn: process.env.VENCIMIENTO
  });
}
module.exports = app;
