const express = require('express')
const Usuario = require('../models/usuario.js')
const app = express()
const bcrypt = require('bcrypt');
const _ = require('lodash');
const {verificarToken,verificarPermisoAdmin} = require('../middlewares/Auth');

app.get('/', function (req, res) {
  res.json('Hello World')
})

app.get('/usuario',verificarToken,(req,res)=>{
    let desde = Number(req.query.desde) || 0;
    let limite = Number(req.query.limite) || 5;
    Usuario.find({estado:true})
        .skip(desde)
        .limit(limite)
        .exec((err,usuarios)=>{
            if (err) {
                return res.status(400).json({
                    ok:false,
                    err
                })
            }
            Usuario.count({estado:true},(err,conteo)=>{
                res.status(200).json({
                    ok:true,
                    cantidad:conteo,
                    usuarios
                })

            })
        })
})
app.get('/usuario/:id',(req,res)=> {
    let id = req.params.id;
    res.send('get usuario')

})
app.post('/cargarUsuarios',[verificarToken,verificarPermisoAdmin],(req,res)=>{
    for (let i = 0; i < 16; i++){
        let usuario = new Usuario({
            nombre:'test'+i,
            email:`test${i}@gmail.com`,
            pass:bcrypt.hashSync('213',10)
        })
        usuario.save();
    }
    res.json('ok')
})
app.post('/usuario',[verificarToken,verificarPermisoAdmin],(req,res)=> {
    let body = req.body;
    body.pass = bcrypt.hashSync(body.pass,10);
    let usuario = new Usuario(body);
    usuario.save((err,usuarioDB)=>{
        if (err) {
            return res.status(400).json({
                ok:false,
                err
            })
        }
        res.status(200).json({
            ok:true,
            mensaje:usuarioDB
        })
    })
})

app.put('/usuario/:id',[verificarToken,verificarPermisoAdmin],(req,res)=> {
    let id = req.params.id;
    let body = _.pick(req.body,['nombre','email','img','role','estado']);

    Usuario.findByIdAndUpdate(id,body,{new:true,runValidators:true},(err,userBD)=>{
        if (err) {
            return res.status(400).json({
                ok:false,
                err
            })
        }
        res.status(200).json({
            ok:true,
            userBD
        })
    })

})

app.delete('/usuario/:id',[verificarToken,verificarPermisoAdmin],(req,res)=> {
    let id = req.params.id;

    Usuario.findByIdAndUpdate(id,{estado:false},{new:true},(err,userBD)=>{
        if (err) {
            return res.status(400).json({
                ok:false,
                err
            })
        }
        res.status(200).json({
            ok:true,
            userBD
        })
    })

})

module.exports = app;
