// =============================
// puerto
// =============================
process.env.PORT = process.env.PORT || 80;
// =============================
// entorno
// =============================
process.env.NODE_ENV = process.env.NODE_ENV || 'dev';

// =============================
// vencimiento
// =============================

process.env.VENCIMIENTO = 60 * 60 * 24 * 30;


process.env.SEED = process.env.SEED || 'este-es-un-seed-desa';


// =============================
// base de datos
// =============================

let urlDB,seed;

if(process.env.NODE_ENV === "dev"){
    urlDB = 'mongodb://localhost:27017/cafe';
}else{
    urlDB = process.env.URLDB;
}
process.env.DB = urlDB;


// =============================
// google client id
// =============================

process.env.CLIENT_ID = process.env.CLIENT_ID || '1031651596763-vggapuulgmseu4200ob3444eu0scr9op.apps.googleusercontent.com';
